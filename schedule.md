# Schedule

Days will start at 9am and end at 6pm.

## Sunday 22/11/2015
Arrival of the students

## Monday 23/11/2015
* Morning:
  * General introduction: aim of the course, practicalities, introduction to lecturers and TAs, ...
  * Introduction to data visualization (Jan A)
* Afternoon:
  * Introduction to design (Francis R)
  * Redesign exercises
  * Setting up computers
## Tuesday 24/11/2015
* Morning:
  * Data cleaning and preparation: OpenRefine (Herwig VM)
  * D3: general introduction (Eamonn M)
* Afternoon:
  * D3: data binding (Eamonn M)

## Wednesday 25/11/2015
* Morning:
  * D3: ?
* Afternoon:
  * D3: ?
  * discussion on projects

## Thursday 26/11/2015
* Whole day: projects

## Friday 27/11/2015
* Whole day: projects
* Afternoon: project presentations
