# Data Visualization Summer School

This repository accompanies the Biological Data Visualization Summer School held in Leuven, Belgium, and organized by KU Leuven, the European Bioinformatics Institute, and the Flemish Institute for Biotechnology VIB.

## Schedule

For the last version of the schedule, see https://docs.google.com/drawings/d/14wxohK6vc59ecSKBhyvDXROsJcTalxX_ta_tfSsRST4/edit?usp=sharing

## Location

Here's where the course is being held: http://vda-lab.be/contact.html

## Contact

jan.aerts@kuleuven.be
