##HTML & CSS
- [Code Academy](https://www.codecademy.com/tracks/web) - If you're a complete newbie to web development, you should have a go at this before attending!

##JavaScript
- [Eloquent JavaScript](http://eloquentjavascript.net/)

##D3

- [D3 for mere mortals](http://www.recursion.org/d3-for-mere-mortals/)
- [D3, Conceptually](http://code.hazzens.com/d3tut/lesson_0.html)
- [D3 Tutorial Videos by Ian Johnson - Vol I](http://enjalot.github.com/dot-enter/)
- [D3 Tutorial Videos by Ian Johnson - Vol II](http://enjalot.github.io/dot-append/)
- [D3 Tips and Tricks](http://www.d3noob.org/)

##Debugging the Web
- [Web developer extensions for the browser](http://macwright.org/enable-web-developer-extensions/)

##Software

Tools are important, but for this course we'll not need anything too complex. For this course, you'll need: 

- a text editor of choice (our choice would be [Atom](https://atom.io/))
- Python (this is normally installed already, but please check.) - From your terminal type python and if you don't go in to a python console, then you don't have python installed. There are installation instructions on the [Python](https://www.python.org/) website. 
- [Git](https://git-scm.com/) (and Github/Bitbucket) are great for managing your code. We'll use git mainly to pull example code from a repository, but it's useful to have and know anyway. A nice cheat sheet on git is [here](http://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf) and there are many great tutorials online already.

##Aesthetic Apps

Making your visualization look good is nearly as important as making it functional. Unless things are clear and well laid out, no one will really care what awesome tool you've built.
Luckily, there are some resources to help you create beautiful software :)

- [Material Design Lite](http://www.getmdl.io/index.html)
- [Bootstrap](http://getbootstrap.com/)
- [Font Awesome](https://fortawesome.github.io/Font-Awesome/)